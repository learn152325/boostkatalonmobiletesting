import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('TC-06 Login with valid user'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Object Repository/SauceDemoApp/AddToCart/tapImageJacket'), 0)

Mobile.tap(findTestObject('Object Repository/SauceDemoApp/AddToCart/btnAddToCart'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/SauceDemoApp/AddToCart/addCartIncrase'), 0)

Mobile.tap(findTestObject('Object Repository/SauceDemoApp/AddToCart/btnCartPage'), 0)

Mobile.tap(findTestObject('Object Repository/SauceDemoApp/AddToCart/btnCheckout'), 0)

Mobile.verifyElementExist(findTestObject('SauceDemoApp/CheckoutPage/checkoutTextPage'), 0)

Mobile.verifyElementText(findTestObject('SauceDemoApp/CheckoutPage/btnToPayment'), 'To Payment')

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

Mobile.closeApplication()

